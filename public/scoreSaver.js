function SaveScore() {
  var name = $("#name-input").val();

  if (name){
    console.log("Saving score for " + name);
    var db = firebase.firestore();
  
    db.collection("players").add({
        name: name,
        score: 6
    })
    .then(function(docRef) {
        console.log("Document written with ID: ", docRef.id);
    })
    .catch(function(error) {
        console.error("Error adding document: ", error);
    });
  }
  else {
    console.log("enter a name");
  }
}

function showHighScores() {
  var db = firebase.firestore();

  db.collection("players").get()
  .then(function(querySnapshot) {
    querySnapshot.forEach((doc) => {
        console.log(`${doc.id} => ${doc.data()}`);
        console.log(doc.data);
    });
  })
}