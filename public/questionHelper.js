const request = new XMLHttpRequest();

function FetchAQuestion() {
  var categoryId = $("#category-select").val();
  console.log(categoryId);

  request.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        var response = JSON.parse(request.responseText);
        var result = response.results[0];

        UpdateHtml(result)
      }
  };

  request.open("GET", "https://opentdb.com/api.php?amount=10&category=" + categoryId, true);
  request.send();
}

const UpdateHtml = function(result) {
  document.getElementById("question").innerHTML = result.question;
  document.getElementById("difficulty").innerHTML = result.difficulty;

  var answers = [];
  answers.push(result.correct_answer);
  result.incorrect_answers.forEach(element => {
    answers.push(element);
  });

  document.getElementById("q1").innerHTML = '<button onclick ="checkanswer(true)" >'+answers[0]+'</button>';
  document.getElementById("q2").innerHTML = '<button onclick ="checkanswer()">'+answers[1]+'</button>';
  document.getElementById("q3").innerHTML = '<button onclick ="checkanswer()">'+answers[2]+'</button>';
  document.getElementById("q4").innerHTML = '<button onclick ="checkanswer()">'+answers[3]+'</button>';
}

function checkanswer(correct) {
  if (correct == true) {
    document.getElementById("result").innerHTML = "Correct :)";
  
  } else {
    document.getElementById("result").innerHTML = "Incorrect :(";
  }

}